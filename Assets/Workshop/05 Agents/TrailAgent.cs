﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using EasyButtons;

public class TrailAgent : MonoBehaviour {
    [Header("Trail Agent Parameters")] 
    [SerializeField, Range(1, 50000)] private int _agentsCount = 1;
    [SerializeField, Range(0f, 1f)] private float _trailDecayFactor = 0.9f;
    [SerializeField, Range(0, 100)] private int _neighbourhoodRange = 1;
    [SerializeField, Range(-1, 1)] private float _seekingDot = 0f;
    [SerializeField, Range(0, 1)] private float _trailThreshold = 0.1f;
    [SerializeField] private bool _debug = false;
    [SerializeField] private float _brushSize = 10f;
    [SerializeField] private int _numCols = 256;
    [SerializeField] private Gradient _trailDecayGradient = new Gradient();
    
    [Header("Setup")] 
    [SerializeField, Range(8, 2048)] private int _res;
    [SerializeField, Range(0, 50)] private int _stepsPerFrame = 1;
    [SerializeField, Range(1, 50)] private int _stepMod = 5;
    [SerializeField] private Material _outMat = null;
    [SerializeField] private ComputeShader _cs = null;

    private RenderTexture _readTex = null;
    private RenderTexture _writeTex = null;
    private RenderTexture _outTex = null;
    private RenderTexture _debugTex = null;
    
    private ComputeBuffer _agentsBuffer = null;
    private int _agentsDebugKernel = 0;
    private int _moveAgentsKernel = 0;
    private int _writeTrailsKernel = 0;
    private int _renderKernel = 0;
    private int _diffuseTextureKernel = 0;
    
    private List<ComputeBuffer> _buffers = new List<ComputeBuffer>();
    private List<RenderTexture> _textures = new List<RenderTexture>();
    private int _stepN = -1;

    private void Start() {
        Reset();
    }

    private void Update() {
        if (Time.frameCount % _stepMod == 0) {
            for (int i = 0; i < _stepsPerFrame; i++) {
                Step();
            }
        }
    }

    private void OnDisable() {
        Release();
    }

    private void OnDestroy() {
        Release();
    }

    private void OnEnable() {
        Release();
    }

    [Button]
    public void Reset() {
        Release();
        _moveAgentsKernel = _cs.FindKernel("MoveAgentsKernel");
        _writeTrailsKernel = _cs.FindKernel("WriteTrailsKernel");
        
        _agentsDebugKernel = _cs.FindKernel("AgentsDebugKernel");
        _renderKernel = _cs.FindKernel("RenderKernel");
        _diffuseTextureKernel = _cs.FindKernel("DiffuseTextureKernel");
        
        _readTex = CreateTexture(_res, FilterMode.Point);
        _writeTex = CreateTexture(_res, FilterMode.Point);
        _outTex = CreateTexture(_res, FilterMode.Point);
        _debugTex = CreateTexture(_res, FilterMode.Point);
        
        _agentsBuffer = new ComputeBuffer(_agentsCount, sizeof(float) * 4);
        _buffers.Add(_agentsBuffer);

        GPUResetKernel();
        Render();
    }

    [Button]
    void Step() {
        _stepN += 1;
        _cs.SetInt("time", Time.frameCount);
        _cs.SetInt("stepN", _stepN);
        
        GPUMoveAgentsKernel();

        if (_stepN % 2 == 1) {
            GPUDiffuseTextureKernel();
            GPUWriteTrailsKernel();
            SwapTex();
        }
        
        Render();
    }

    private void GPUMoveAgentsKernel() {
        _cs.SetBuffer(_moveAgentsKernel, "agentsBuffer", _agentsBuffer);
        _cs.SetTexture(_moveAgentsKernel, "readTex", _readTex);
        _cs.SetTexture(_moveAgentsKernel, "debugTex", _debugTex);
        _cs.SetInt("debug", _debug ? 1 : 0);
        _cs.SetInt("neighbourhoodRange", _neighbourhoodRange);
        _cs.SetFloat("seekingDot", _seekingDot);
        _cs.SetFloat("trailThreshold", _trailThreshold);
        
        _cs.Dispatch(_moveAgentsKernel, _agentsCount, 1, 1);
    }
    
    private void GPUResetKernel() {
        int kernel;
        
        _cs.SetInt("res", _res);
        _cs.SetInt("time", Time.frameCount);

        kernel = _cs.FindKernel("ResetTextureKernel");
        _cs.SetTexture(kernel, "writeTex", _writeTex);
        _cs.Dispatch(kernel, _res, _res, 1);
        
        _cs.SetTexture(kernel, "writeTex", _readTex);
        _cs.Dispatch(kernel, _res, _res, 1);

        kernel = _cs.FindKernel("ResetAgentsKernel");
        _cs.SetBuffer(kernel, "agentsBuffer", _agentsBuffer);
        _cs.Dispatch(kernel, _agentsCount, 1, 1);
    }

    private void GPUDiffuseTextureKernel() {
        _cs.SetTexture(_diffuseTextureKernel, "readTex", _readTex);
        _cs.SetTexture(_diffuseTextureKernel, "writeTex", _writeTex);
        _cs.SetFloat("trailDecayFactor", _trailDecayFactor);
        
        var cols = new Vector4[_numCols];
        var step = 1f / (_numCols - 1);
        for (int c = 0; c < cols.Length; c++) {
            cols[c] = _trailDecayGradient.Evaluate(c * step);
        }
        
        _cs.SetVectorArray("colors", cols);
        _cs.SetFloat("brushSize", _brushSize);
        _cs.SetVector("mPos", Input.mousePosition);
        _cs.Dispatch(_diffuseTextureKernel, _res, _res, 1);
    }
    
    private void GPUWriteTrailsKernel() {
        _cs.SetBuffer(_writeTrailsKernel, "agentsBuffer", _agentsBuffer);
        
        _cs.SetTexture(_writeTrailsKernel, "writeTex", _writeTex);
        _cs.Dispatch(_writeTrailsKernel, _agentsCount, 1 ,1);
    }

    private void SwapTex() {
        var tmp = _readTex;
        _readTex = _writeTex;
        _writeTex = tmp; 
    }
    
    private void Render() {
        GPURenderKernel();
        if (_debug) {
            GPUAgentsDebugKernel();
        }
        
        _outMat.SetTexture("_UnlitColorMap", _outTex);
    }

    private void GPURenderKernel() {
        _cs.SetTexture(_renderKernel, "readTex", _readTex);
        _cs.SetTexture(_renderKernel, "outTex", _outTex);
        _cs.SetTexture(_renderKernel, "debugTex", _debugTex);
        _cs.Dispatch(_renderKernel, _res, _res, 1);
    }
    
    private void GPUAgentsDebugKernel() {
        _cs.SetBuffer(_agentsDebugKernel, "agentsBuffer", _agentsBuffer);
        _cs.SetTexture(_agentsDebugKernel, "outTex", _outTex);
        
        _cs.Dispatch(_agentsDebugKernel, _agentsCount, 1, 1);
    }
    

    private static RenderTexture CreateTexture(int res, FilterMode filterMode) {
        var tex = new RenderTexture(
            res,
            res,
            1,
            RenderTextureFormat.ARGBFloat) {
            name = "out",
            enableRandomWrite = true,
            dimension = UnityEngine.Rendering.TextureDimension.Tex2D,
            volumeDepth = 1,
            filterMode = filterMode,
            wrapMode = TextureWrapMode.Repeat,
            autoGenerateMips = false,
            useMipMap = false
        };

        tex.Create();

        return tex;
    }

    private void Release() {
        foreach (var buffer in _buffers.Where(buffer => buffer != null)) {
            buffer.Release();
        }
        
        _buffers.Clear();
        
        foreach (var tex in _textures.Where(tex => tex != null)) {
            tex.Release();
        }
        
        _textures.Clear();
    }
    
}
