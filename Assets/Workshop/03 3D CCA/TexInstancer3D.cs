﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class TexInstancer3D : MonoBehaviour
{
    public Mesh mesh;
    public Material material;
    public CCA3D cca;

    [Range(0, .3f)]
    public float size = .5f;

    [Range(0, 1f)]
    public float spacing = .5f;


    ComputeBuffer argBuffer;
    private uint[] args = new uint[5] { 0, 0, 0, 0, 0 };

    readonly Bounds bounds = new Bounds(Vector3.zero, Vector3.one * 100);


    void Update()
    {
        if(argBuffer != null)
        {
            argBuffer.Release();
        }


        RenderTexture tex = cca.outTex;
        int rez = tex.width;
        

        argBuffer = new ComputeBuffer(1, 5 * sizeof(uint), ComputeBufferType.IndirectArguments);
        args[0] = mesh.GetIndexCount(0);
        args[1] = (uint)(rez * rez * rez);
        args[2] = mesh.GetIndexStart(0);
        args[3] = mesh.GetBaseVertex(0);
        argBuffer.SetData(args);

        material.SetInt("rez", rez);
        material.SetTexture("tex", tex);
        material.SetFloat("size", size);
        material.SetFloat("spacing", spacing);
        material.SetMatrix("mat", transform.localToWorldMatrix);
        material.SetVector("position", transform.position);

        Graphics.DrawMeshInstancedIndirect(mesh, 0, material, bounds, argBuffer);

    }
}
