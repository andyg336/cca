﻿#pragma kernel ResetKernel
#pragma kernel StepKernel

Texture3D<float> readTex;
RWTexture3D<float> writeTex;
RWTexture3D<float4> outTex;

int range;
uint threshold;
uint nstates;
bool moore;

int rez;


/*
 *
 *
 *  RESET
 *
 *
 */

 // via "The Art of Code" on Youtube
float2 Random(float2 p) {
	float3 a = frac(p.xyx * float3(123.34, 234.34, 345.65));
	a += dot(a, a + 34.45);
	return frac(float2(a.x * a.y, a.y * a.z));
}

[numthreads(1, 1, 1)]
void ResetKernel(uint3 id : SV_DispatchThreadID)
{
	writeTex[id.xyz] = (int)(Random(id.xy + id.z*.1).x * nstates);
}


/*
 *
 *
 *  STEP
 *
 *
 */

 //  Function from Iñigo Quiles
 //  https://www.shadertoy.com/view/MsS3Wc
 //  via: https://thebookofshaders.com/06/
float4 hsb2rgb(float3 c) {
	float3 rgb = clamp(abs(((c.x * 6.0 + float3(0.0, 4.0, 2.0)) % 6.0) - 3.0) - 1.0, 0.0, 1.0);
	rgb = rgb * rgb * (3.0 - 2.0 * rgb);
	float3 o = c.z * lerp(float3(1.0, 1.0, 1.0), rgb, c.y);
	return float4(o.r, o.g, o.b, 1);
}

void Render(uint3 id, int state, int count)
{

	float s = state / (float)nstates;
	float c = count / ((float)threshold);
	////////////////////////////////////////////

	float3 hsb;
	// Range Color
	if (true) {
		hsb.x = hsb.y = hsb.z = s;
		hsb.x = lerp(0, 1, hsb.x);
		hsb.y += .7;
		hsb.z = state % 2 == 0 && (state % 3 ==0 || state % 4 ==0);
		outTex[id.xyz] = hsb2rgb(hsb);
	}

	////////////////////////////////////////////
	// Crop
	float d = distance(float3(rez / 2.0, rez / 2.0, rez / 2.0), id.xyz) / (rez / 2.0);
	outTex[id.xyz] *= smoothstep(.95, .9, d);

}

[numthreads(8, 8, 8)]
void StepKernel(uint3 id : SV_DispatchThreadID)
{
	// READ STATE
	uint state = readTex[id.xyz];

	uint count = 0;
	uint next = state + 1 == nstates ? 0 : state + 1; // (preserves higher states)

	// DETERMINE WHETHER EACH DIRECTION CONTAINS NEXT 
	for (int x = -range; x <= range; x++) {
		for (int y = -range; y <= range; y++) {
			for (int z = -range; z <= range; z++) {

				// ignore self
				if ((x == 0 && y == 0 && z == 0)) {
					continue;
				}

				if (moore || (x == 0 || y == 0 || z == 0)) {
					count += (uint) readTex[id.xyz + uint3(x, y, z)] == next;
				}
			}
		}
	}

	// IF THRESHOLD IS REACHED, INCREMENT STEP
	if (count >= (threshold)) {
		state = (state + 1) % nstates;
	}

	// WRITE STATE
	writeTex[id.xyz] = state;

	Render(id, state, count);
}


