﻿using EasyButtons;
using UnityEngine;

[ExecuteAlways]
public class CCA3D : MonoBehaviour {
   private const int MAX_RANGE = 10;
   private const int MAX_THRESHOLD = 40;
   private const int MAX_STATES = 40;
   private const int THREAD_SIZE = 8;
   
   [Header("CCA Primary Params")] 
   [SerializeField, Range(1, MAX_RANGE)] private int _range = 1;
   [SerializeField, Range(0, MAX_THRESHOLD)] private int _threshold = 3;
   [SerializeField, Range(0, MAX_STATES)] private int _nStates = 3;
   [SerializeField] private bool _moore = false;
   
   [Header("CCA Secondary Params")] 
   [SerializeField, Range(1, MAX_RANGE)] private int _range2 = 1;
   [SerializeField, Range(0, MAX_THRESHOLD)] private int _threshold2 = 3;
   [SerializeField, Range(0, MAX_STATES)] private int _nStates2 = 3;
   [SerializeField] private bool _moore2 = false;
   
   [Header("Setup")]
   [SerializeField, Range(0, 7)] private int _colorMethod = 0;
   [SerializeField, Range(0, 2048)] private int _resolution = 128;
   [SerializeField, Range(0, 50)] private int _stepsPerFrame = 0;
   [SerializeField, Range(1, 50)] private int _stepMod = 1;
   [SerializeField] private ComputeShader _cs = null;
   
   private int DispatchSize => _resolution / THREAD_SIZE;
   private RenderTexture _readTex = null;
   private RenderTexture _outTex = null;
   private RenderTexture _writeTex = null;
   private int _stepKernel;

   public static RenderTexture OutTex {
      get {
         if (_instance) {
            if (_instance._outTex) {
               return _instance._outTex;
            }
            Debug.LogError("No reference to Texture");
            return null;
         }
         Debug.LogError("No instance of CCA3D");
         return null;
      }
   }

   private static CCA3D _instance;
   
   private void Start() {
      if (_instance) {
         Destroy(_instance);
      }
      _instance = this;
      Reset();
   }
   
   private void Update() {
      if (Time.frameCount % _stepMod == 0) {
         for (int i = 0; i < _stepsPerFrame; i++) {
            Step();
         }
      }
   }   

   private void OnValidate() {
      SetPrimaryParams();
   }

   [Button]
   private void Reset() {
      _readTex = Create3DTexture(RenderTextureFormat.RFloat);
      _writeTex = Create3DTexture(RenderTextureFormat.RFloat);
      _outTex = Create3DTexture(RenderTextureFormat.ARGBFloat);

      _stepKernel = _cs.FindKernel("StepKernel");

      GPUResetKernel();
   }

   private RenderTexture Create3DTexture(RenderTextureFormat format) {
      var tex = new RenderTexture(_resolution,_resolution, 0, format) {
         enableRandomWrite = true, 
         filterMode = FilterMode.Point, 
         wrapMode = TextureWrapMode.Repeat, 
         useMipMap = false,
         volumeDepth = _resolution,
         dimension = UnityEngine.Rendering.TextureDimension.Tex3D
      };
      tex.Create();

      return tex;
   }

   private void GPUResetKernel() {
      int k = _cs.FindKernel("ResetKernel");
      _cs.SetTexture(k, "writeTex", _writeTex);
      _cs.SetInt("res", _resolution);
      
      _cs.SetInt("range", _range);
      _cs.SetInt("threshold", _threshold);
      _cs.SetInt("nStates", _nStates);
      _cs.SetInt("colorMethod", _colorMethod);
      _cs.SetBool("moore", _moore);
      
      _cs.Dispatch(k, DispatchSize, DispatchSize, DispatchSize);
      SwapTex();
   }

   [Button]
   private void AddNoise() {
      int k = _cs.FindKernel("SecondaryNoiseKernel");
      _cs.SetTexture(k, "readTex", _readTex);
      _cs.SetTexture(k, "writeTex", _writeTex);
      
      _cs.Dispatch(k, DispatchSize, DispatchSize, DispatchSize);
      
      SwapTex();
   }

   [Button]
   private void SetPrimaryParams() {
      SetParams(_range, _threshold, _nStates, _moore);
   }
   
   [Button]
   private void SetSecondaryParams() {
      SetParams(_range2, _threshold2, _nStates2, _moore2);
   }
   
   [Button]
   private void RandomisePrimaryParams() {
      RandomiseParams(out _range, out _threshold, out _nStates, out _moore);
   }

   [Button]
   private void RandomiseSecondaryParamsWithNoise() {
      RandomiseParams(out _range2, out _threshold2, out _nStates2, out _moore2);
      AddNoise();
   }

   private void RandomiseParams(out int range, out int threshold, out int nStates, out bool moore) {
      var rand = new System.Random();
      range = (int) (rand.NextDouble() * (MAX_RANGE - 1)) + 1;
      threshold = (int) (rand.NextDouble() * (MAX_THRESHOLD - 1)) + 1;
      nStates = (int) (rand.NextDouble() * (MAX_STATES - 2)) + 2;
      moore = rand.NextDouble() <= 0.5f;

      SetParams(range, threshold, nStates, moore);
   }

   private void SetParams(int range, int threshold, int nStates, bool moore) {
      _cs.SetInt("range", range);
      _cs.SetInt("threshold", threshold);
      _cs.SetInt("nStates", nStates);
      _cs.SetBool("moore", moore);
   }
   
   [Button]
   private void ResetAndRandomise() {
      RandomisePrimaryParams();
      Reset();
   }
   
   [Button]
   private void Step() {
      _cs.SetTexture(_stepKernel, "readTex", _readTex);
      _cs.SetTexture(_stepKernel, "writeTex", _writeTex);
      _cs.SetTexture(_stepKernel, "outTex", _outTex);
      _cs.SetInt("colorMethod", _colorMethod);

      _cs.Dispatch(_stepKernel, DispatchSize, DispatchSize, DispatchSize);

      SwapTex();
   }

   private void SwapTex() {
      var tmp = _readTex;
      _readTex = _writeTex;
      _writeTex = tmp;
   }
   
}
