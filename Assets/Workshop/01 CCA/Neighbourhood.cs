﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public unsafe struct NeighbourhoodPayload {
    public uint NSize;
    public fixed int AddressX[240];
    public fixed int AddressY[240];
    public int DRangeLow;
    public int DRangeHigh;
    public int LRangeLow;
    public int LRangeHigh;
    public Color debugCol;
}

[Serializable]
public class Neighbourhood {
    public bool[] Address1D;
    public bool[,] CellEnabled;
    public int ActiveCellCount;
    [Range(-1,840)] public int LifeRangeLow;
    [Range(-1,840)] public int LifeRangeHigh;
    [Range(-1,840)] public int DeathRangeLow;
    [Range(-1,840)] public int DeathRangeHigh;

    private readonly HashSet<Vector4> _addresses = new HashSet<Vector4>();
    private const int MaxLength = 29;
    private const int MaxCount = 841;
    private const int MaxFlattenedSize = 253;

    public Neighbourhood() {
        Address1D = new bool[MaxCount];
        CellEnabled = new bool[MaxLength,MaxLength];
    }
    
    public void SetAddress(int x, int y, bool state) {
        var prevState = CellEnabled[x, y];
        CellEnabled[x, y] = state;
        if (state && !prevState) {
            ActiveCellCount++;
        } else if (!state && prevState) {
            ActiveCellCount--;
        }
    }
   
    public Vector4[] GetAddresses() {
        _addresses.Clear();
        var count = MaxFlattenedSize;
        for (int x = 0; x < CellEnabled.GetLength(0); x++) {
            for (int y = 0; y < CellEnabled.GetLength(1); y++) {
                if (count <= 0) {
                    Debug.LogWarningFormat(
                        "Returning early as we've hit our {0} address limit!", MaxFlattenedSize);
                    return _addresses.ToArray();
                }
                if (CellEnabled[x, y]) {
                    _addresses.Add(new Vector2(x-14f, y-14f));
                    count--;
                }
            }
        }

        return _addresses.ToArray();
    }
}