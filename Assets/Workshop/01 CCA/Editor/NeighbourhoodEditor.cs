﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MNCA2D))]
public class NeighbourhoodEditor : Editor {
   private Neighbourhood[] _neighbourhoods = null; // 4 Neighbourhoods
   private NeighbourhoodNumber _currentNeighbourhood;
   private const int _unitSize = 14;
   private const int _gridSize = 29;
   private MNCA2D _mnca = null;

   enum NeighbourhoodNumber { 
      n01 = 0, 
      n02 = 1, 
      n03 = 2,
      n04 = 3
   }

   public Neighbourhood[] Neighbourhoods {
      get {
         if (_neighbourhoods == null) {
            _neighbourhoods = new Neighbourhood[4];
            for (var i = 0; i < _neighbourhoods.Length; i++) {
               _neighbourhoods[i] = new Neighbourhood();
            }

            _mnca.Neighbourhoods = _neighbourhoods;
         }

         return _neighbourhoods;
      }
   }

   public override void OnInspectorGUI() {
      if (_mnca == null) {
         _mnca = (MNCA2D) serializedObject.targetObject;
      }

      DrawDefaultInspector();
      if (GUILayout.Button("Reset")) {
         _mnca.Reset();
      }
      // Generic Params
      if (GUILayout.Button("Add Noise")) {
         _mnca.AddNoise();
      }
      // Colour Params
         if (GUILayout.Button("Set Colours")) {
            _mnca.SetColors();
         }
      // MNCA Params
      _currentNeighbourhood = (NeighbourhoodNumber)EditorGUILayout.EnumPopup(
         "Neighbourhood", _currentNeighbourhood);
      if (GUILayout.Button("Load All")) {
         LoadNeighbourhood(0, NeighbourhoodNumber.n01.ToString());
         LoadNeighbourhood(1, NeighbourhoodNumber.n02.ToString());
         LoadNeighbourhood(2, NeighbourhoodNumber.n03.ToString());
         LoadNeighbourhood(3, NeighbourhoodNumber.n04.ToString());
      }
      GUILayout.BeginHorizontal();
      if (GUILayout.Button("Save Neighbourhood")) {
         SaveNeighbourhood((int)_currentNeighbourhood, _currentNeighbourhood.ToString());
      }
      if (GUILayout.Button("Load Neighbourhood")) {
         LoadNeighbourhood((int)_currentNeighbourhood, _currentNeighbourhood.ToString());
      }
      GUILayout.EndHorizontal();
      GUI.backgroundColor = Color.red;
      if (GUILayout.Button("Clear Neighbourhood")) {
         ClearNeighbourhood((int)_currentNeighbourhood);
      }
      GUI.backgroundColor = Color.white;
      if (GUILayout.Button("Set Params")) {
         _mnca.SetPrimaryParams();
      }
      if (GUILayout.Button("Randomise Colours")) {
         _mnca.RandomizeColors();
      }
      // Taken from @lubblub's MNCA editor inspiration and snippet
      var cellEnabled = Neighbourhoods[(int) _currentNeighbourhood].CellEnabled;
      for (int x = 0; x < _gridSize; x++) {
         GUILayout.BeginHorizontal();

         for (int y = 0; y < _gridSize; y++) {
            if(x == 14 && y == 14){
               GUI.backgroundColor = Color.green;
               GUILayout.Button("",
                  GUILayout.Height(_unitSize),
                  GUILayout.Width(_unitSize));
            }
            else {
               GUI.backgroundColor = cellEnabled[x, y] ? Color.white : Color.black;

               if (GUILayout.Button("",
                  GUILayout.Width(_unitSize),
                  GUILayout.Height(_unitSize))) {
                  var state = cellEnabled[x, y] = !cellEnabled[x, y];
                  Neighbourhoods[(int)_currentNeighbourhood].SetAddress(x, y, state);
                  _mnca.SetPrimaryParams();
               }
            }
         }

         GUILayout.EndHorizontal();
      }
      var activeCellCount = Neighbourhoods[(int) _currentNeighbourhood].ActiveCellCount;
      GUILayout.Label("Active Cells: " + activeCellCount);
   }

   private void SaveNeighbourhood(int nId, string fileName) {
      var cellEnabled = Neighbourhoods[nId].CellEnabled;
      var xLen = cellEnabled.GetLength(0);
      var yLen = cellEnabled.GetLength(1);
      var count = 0;
      // Flatten addresses into a 1D representation
      for (int y = 0; y < yLen; y++) {
         for (int x = 0; x < xLen; x++) {
               Neighbourhoods[nId].Address1D[count] = cellEnabled[x, y];
            count++;
         }
      }
      
      // Cache neighbourhood rules
      Neighbourhoods[nId].LifeRangeHigh = _mnca.NeighbourhoodRules[nId].LifeRangeHigh;
      Neighbourhoods[nId].LifeRangeLow = _mnca.NeighbourhoodRules[nId].LifeRangeLow;
      Neighbourhoods[nId].DeathRangeHigh = _mnca.NeighbourhoodRules[nId].DeathRangeHigh;
      Neighbourhoods[nId].DeathRangeLow = _mnca.NeighbourhoodRules[nId].DeathRangeLow;
      
      // Convert and write file to json
      var json = JsonUtility.ToJson(Neighbourhoods[nId]);
      var path = Application.dataPath + "/" + fileName + ".json";
      File.WriteAllText(
         path, 
         json);
   }

   private void LoadNeighbourhood(int nId, string fileName) {
      var json = File.ReadAllText(
         Application.dataPath + "/" + fileName + ".json");

      var neighbourHood = JsonUtility.FromJson<Neighbourhood>(json);
      // Unpack addresses
      var cellEnabled = Neighbourhoods[nId].CellEnabled;
      var xLen = cellEnabled.GetLength(0);
      var yLen = cellEnabled.GetLength(1);
      Neighbourhoods[nId].ActiveCellCount = 0;
      for (int y = 0; y < yLen; y++) {
         for (int x = 0; x < xLen; x++) {
            var state = neighbourHood.Address1D[y * xLen + x];
            Neighbourhoods[nId].SetAddress(x, y, state);
         if (state)
            Neighbourhoods[nId].SetAddress(x, y, cellEnabled[x, y]);
         }
      }
      
      // Load neighbourhood rules
      _mnca.NeighbourhoodRules[nId].LifeRangeHigh = neighbourHood.LifeRangeHigh;
      _mnca.NeighbourhoodRules[nId].LifeRangeLow = neighbourHood.LifeRangeLow;
      _mnca.NeighbourhoodRules[nId].DeathRangeHigh = neighbourHood.DeathRangeHigh;
      _mnca.NeighbourhoodRules[nId].DeathRangeLow = neighbourHood.DeathRangeLow;
      
      Debug.Log("Loaded Neighbourhood addresses");
   }

   private void ClearNeighbourhood(int nId) {
      var cellEnabled = Neighbourhoods[nId].CellEnabled;
      for (int x = 0; x < cellEnabled.GetLength(0); x++) {
         for (int y = 0; y < cellEnabled.GetLength(1); y++) {
            cellEnabled[x, y] = false;
            Neighbourhoods[nId].SetAddress(x, y, cellEnabled[x, y]);
         }
      }
   }
}
