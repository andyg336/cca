﻿using UnityEngine;
using Randoms = System.Random;

[System.Serializable]
public struct NeighbourhoodRules {
   [Range(-1, 200)] public int LifeRangeLow;
   [Range(-1, 200)] public int LifeRangeHigh;
   [Range(-1, 200)] public int DeathRangeLow;
   [Range(-1, 200)] public int DeathRangeHigh;
}

[ExecuteAlways]
public class MNCA2D : MonoBehaviour {
   private const int MAX_THRESHOLD = 25;
   private const int MAX_STATES = 20;
   private const int THREAD_SIZE = 32;
   
   [Header("Primary Params")] 
   [SerializeField, Range(0, MAX_THRESHOLD)] private int _threshold = 3;
   [SerializeField, Range(0, MAX_STATES)] private int _nStates = 3;
   [SerializeField] public NeighbourhoodRules[] NeighbourhoodRules = new NeighbourhoodRules[2];
   
   [Header("Colour Params")]
   [SerializeField] private Gradient _gradient;
   [SerializeField, Range(-1, 7)] private int _colorMethod = 0;
   [SerializeField, Range(0f, 1f)] private float _maxHue = 1f;
   [SerializeField, Range(0f, 1f)] private float _minHue = 0f;
   [SerializeField, Range(0f, 1f)] private float _maxSaturation = 1f;
   [SerializeField, Range(0f, 1f)] private float _minSaturation = 0.5f;
   [SerializeField, Range(0f, 1f)] private float _maxValue = 1f;
   [SerializeField, Range(0f, 1f)] private float _minValue = 0f;
   [SerializeField] private Color[] _colors;
   
   [Header("Setup")]
   [SerializeField, Range(0, 2048)] private int _resolution = 8;
   [SerializeField, Range(0, 50)] private int _stepsPerFrame = 0;
   [SerializeField, Range(1, 50)] private int _stepMod = 1;
   [SerializeField] private ComputeShader _cs = null;
   [SerializeField] private Material _outMat = null;
   
   private int DispatchSize => _resolution / THREAD_SIZE;

   private RenderTexture _outTex = null;
   private RenderTexture _readTex = null;
   private RenderTexture _writeTex = null;
   private int _stepKernel;
   private ComputeBuffer _buffer;
   public Neighbourhood[] Neighbourhoods;
   
   public static MNCA2D Instance;

   public static RenderTexture OutTex {
      get {
         if (Instance) {
            if (Instance._outTex) {
               return Instance._outTex;
            }
            Debug.LogError("No reference to Texture");
            return null;
         }
         return null;
      }
   }
   
   private void Start() {
      if (Instance) {
         Destroy(Instance);
      }
      Instance = this;
      Reset();
      if (Neighbourhoods == null) {
         InitNeighbourhoods();
      }
   }

   private void InitNeighbourhoods() {
      Neighbourhoods = new Neighbourhood[4];
      for (var i = 0; i < Neighbourhoods.Length; i++) {
         Neighbourhoods[i] = new Neighbourhood();
      }
   }

   private void Update() {
      if (Time.frameCount % _stepMod == 0) {
         for (int i = 0; i < _stepsPerFrame; i++) {
            Step();
         }
      }
   }

   private void OnValidate() {
      if (Neighbourhoods == null) {
         InitNeighbourhoods();
      }
      SetPrimaryParams();
   }

   public void Reset() {
      _readTex = CreateTexture(RenderTextureFormat.RFloat);
      _writeTex = CreateTexture(RenderTextureFormat.RFloat);
      _outTex = CreateTexture(RenderTextureFormat.ARGBFloat);

      _stepKernel = _cs.FindKernel("StepKernel");
      SetPrimaryParams();
      GPUResetKernel();
      SetColors();
   }

   private RenderTexture CreateTexture(RenderTextureFormat format) {
      var tex = new RenderTexture(_resolution, _resolution, 1, format) {
         enableRandomWrite = true, 
         filterMode = FilterMode.Point, 
         wrapMode = TextureWrapMode.Repeat, 
         useMipMap = false
      };
      tex.Create();

      return tex;
   }

   private void GPUResetKernel() {
      int k = _cs.FindKernel("ResetKernel");
      _cs.SetTexture(k, "writeTex", _writeTex);
      _cs.SetInt("res", _resolution);
      _cs.SetInt("threshold", _threshold);
      _cs.SetInt("nStates", _nStates);
      _cs.SetInt("colorMethod", _colorMethod);
      
      _cs.Dispatch(k, DispatchSize, DispatchSize, 1);
      SwapTex();
   }

   public void RandomizeColors() {
      var rand = new Randoms(Time.frameCount);
      int keyCount = 8;
      var c = new GradientColorKey[keyCount];
      var a = new GradientAlphaKey[keyCount];

      for (int i = 0; i < keyCount; i++) {
         var h = (float) rand.NextDouble() * (_maxHue - _minHue) + _minHue;
         var s = (float) rand.NextDouble() * (_maxSaturation - _minSaturation) + _minSaturation;
         var v = (float) rand.NextDouble() * (_maxValue - _minValue) + _minValue;

         var newCol = Color.HSVToRGB(h, s, v);
         c[i].color = newCol;
         c[i].time = a[i].time = (i * (1.0f / keyCount));
         a[i].alpha = 1.0f;
      }
      _gradient.SetKeys(c, a);

      SetColors();
   }

   public void SetColors() {
//      var rand = new Randoms(Time.frameCount);
//
//      var colors = new Vector4[_nStates];
//      for (int i = 0; i < _nStates; i++) {
//         var t = (float) rand.NextDouble();
//         colors[i] = _gradient.Evaluate(t);
//      }

      var cols = new Vector4[_colors.Length];
      for (int c = 0; c < _colors.Length; c++) {
         cols[c] = (Vector4)_colors[c];
      }
      _cs.SetVectorArray("colors", cols);
   }

   public void AddNoise() {
      int k = _cs.FindKernel("SecondaryNoiseKernel");
      _cs.SetTexture(k, "readTex", _readTex);
      _cs.SetTexture(k, "writeTex", _writeTex);
      
      _cs.Dispatch(k, DispatchSize, DispatchSize, 1);
      
      SwapTex();
   }

   public void SetPrimaryParams() {
      SetParams(_threshold, _nStates);
   }
   
   private void RandomisePrimaryParams() {
      RandomiseParams(out _threshold, out _nStates);
   }

   public void RandomiseParams(out int threshold, out int nStates) {
      var rand = new System.Random();
      threshold = (int) (rand.NextDouble() * (MAX_THRESHOLD - 1)) + 1;
      nStates = (int) (rand.NextDouble() * (MAX_STATES - 2)) + 2;

      SetParams(threshold, nStates);
   }
   
   private void SetParams(int threshold, int nStates) {
      _cs.SetInt("threshold", threshold);
      _cs.SetInt("nStates", nStates);
      
      /*
      // Compute Buffers
      var neighbourhood = new []{
         new NeighbourhoodPayload(),
         new NeighbourhoodPayload(),
         new NeighbourhoodPayload(),
         new NeighbourhoodPayload()
      };

      for (var n = 0; n < Neighbourhoods.Length; n++) {
         var addresses = Neighbourhoods[n].GetAddresses();
         neighbourhood[n].NSize = (uint) addresses.Length;
         neighbourhood[n].DRangeHigh = NeighbourhoodRules[n].DeathRangeHigh;
         neighbourhood[n].DRangeLow = NeighbourhoodRules[n].DeathRangeLow;
         neighbourhood[n].LRangeHigh = NeighbourhoodRules[n].LifeRangeHigh;
         neighbourhood[n].LRangeLow = NeighbourhoodRules[n].LifeRangeLow;
         neighbourhood[n].debugCol = Color.magenta;
         // This feels like such an arcane ritual 
         unsafe {
             //"pin" pointers to our fixed neighbourhood addresses in memory
            fixed (int* x = neighbourhood[n].AddressX, y = neighbourhood[n].AddressY) {
               // In case nothing's setup, we need _some_ data
               if (addresses.Length == 0) {
                  addresses = new [] {
                     new Vector2(), 
                  };
               }
               *x = (int)addresses[0].x;
               *y = (int)addresses[0].y;
            }

            for (int i = 0; i < addresses.Length; i++) {
               neighbourhood[n].AddressX[i] = (int)addresses[i].x;
               neighbourhood[n].AddressY[i] = (int)addresses[i].y;
            }
         }
      }
      _buffer = new ComputeBuffer(
         neighbourhood.Length, 
         sizeof(int) * 489);
      _buffer.SetData(neighbourhood);
      _cs.SetBuffer(_stepKernel, "neighbourhood", _buffer);
      _buffer.GetData(neighbourhood);
      _buffer.Release();
*/
      
      // Bufferless MNCA
      var n0Addresses = Neighbourhoods[0].GetAddresses();
      _cs.SetInt("nSize0", n0Addresses.Length);
      _cs.SetVectorArray("address0", n0Addresses);
      _cs.SetInt("dRangeLow0", NeighbourhoodRules[0].DeathRangeLow);
      _cs.SetInt("dRangeHigh0", NeighbourhoodRules[0].DeathRangeHigh);
      _cs.SetInt("lRangeLow0", NeighbourhoodRules[0].LifeRangeLow);
      _cs.SetInt("lRangeHigh0", NeighbourhoodRules[0].LifeRangeHigh);
      
      var n1Addresses = Neighbourhoods[1].GetAddresses();
      _cs.SetInt("nSize1", n1Addresses.Length);
      _cs.SetVectorArray("address1", n1Addresses);
      _cs.SetInt("dRangeLow1", NeighbourhoodRules[1].DeathRangeLow);
      _cs.SetInt("dRangeHigh1", NeighbourhoodRules[1].DeathRangeHigh);
      _cs.SetInt("lRangeLow1", NeighbourhoodRules[1].LifeRangeLow);
      _cs.SetInt("lRangeHigh1", NeighbourhoodRules[1].LifeRangeHigh);
      
      var n2Addresses = Neighbourhoods[2].GetAddresses();
      _cs.SetInt("nSize2", n2Addresses.Length);
      _cs.SetVectorArray("address2", n2Addresses);
      _cs.SetInt("dRangeLow2", NeighbourhoodRules[2].DeathRangeLow);
      _cs.SetInt("dRangeHigh2", NeighbourhoodRules[2].DeathRangeHigh);
      _cs.SetInt("lRangeLow2", NeighbourhoodRules[2].LifeRangeLow);
      _cs.SetInt("lRangeHigh2", NeighbourhoodRules[2].LifeRangeHigh);
      
      var n3Addresses = Neighbourhoods[3].GetAddresses();
      _cs.SetInt("nSize3", n3Addresses.Length);
      _cs.SetVectorArray("address3", n3Addresses);
      _cs.SetInt("dRangeLow3", NeighbourhoodRules[3].DeathRangeLow);
      _cs.SetInt("dRangeHigh3", NeighbourhoodRules[3].DeathRangeHigh);
      _cs.SetInt("lRangeLow3", NeighbourhoodRules[3].LifeRangeLow);
      _cs.SetInt("lRangeHigh3", NeighbourhoodRules[3].LifeRangeHigh);
   }
   
   public void ResetAndRandomise() {
      RandomisePrimaryParams();
      RandomizeColors();
      Reset();
   }
   
   private void Step() {
      _cs.SetTexture(_stepKernel, "readTex", _readTex);
      _cs.SetTexture(_stepKernel, "writeTex", _writeTex);
      _cs.SetTexture(_stepKernel, "outTex", _outTex);
      _cs.SetInt("colorMethod", _colorMethod);
      
      var mPos = new Vector2( 
         Input.mousePosition.x / Screen.width,
         Input.mousePosition.y / Screen.height);
      
      _cs.SetVector("mPos", mPos);
      
      _cs.Dispatch(_stepKernel, DispatchSize, DispatchSize, 1);
      SwapTex();
      
      _outMat.SetTexture("_UnlitColorMap", _outTex);
   }

   private void SwapTex() {
      var tmp = _readTex;
      _readTex = _writeTex;
      _writeTex = tmp;
   }

}
