﻿using System;
using UnityEngine;

[ExecuteAlways]
public class TexInstancer : MonoBehaviour {
    [SerializeField] private Material _material = null;
    [SerializeField] private Mesh _mesh = null;

    [SerializeField, Range(0f, 0.5f)] private float _size = 0.5f;
    [SerializeField, Range(0f, 10f)] private float _sizeY = 1f;
    [SerializeField, Range(0f, 0.1f)] private float _spacing = 0.1f;
    
    private ComputeBuffer _argBuffer = null;
    private uint[] _args = new uint[] {0, 0, 0, 0, 0};
    
    readonly Bounds _bounds = new Bounds(Vector3.zero, Vector3.one * 1000);
    

    private void Update() {
        if (!_material || !_mesh) {
            return;
        }

        _argBuffer?.Release();

        if (!CCA2D.OutTex && !MNCA2D.OutTex) {
            Debug.LogError("Can't access out tex");
            return;
        }
        
#if UNITY_EDITOR
        if(!Application.isPlaying) {
            UnityEditor.SceneView.RepaintAll();
        }
#endif
        var outTex = CCA2D.OutTex ? CCA2D.OutTex : MNCA2D.OutTex;

        int res = outTex.width;
        
        _argBuffer = new ComputeBuffer(
            1, 
            5 * sizeof(uint), 
            ComputeBufferType.IndirectArguments);
        
        _args[0] = _mesh.GetIndexCount(0);
        _args[1] = (uint)res * (uint)res;
        _args[2] = _mesh.GetIndexStart(0);
        _args[3] = _mesh.GetBaseVertex(0);
        
        _argBuffer.SetData(_args);
        
        _material.SetFloat("spacing", _spacing);
        _material.SetFloat("size", _size);
        _material.SetFloat("sizey", _sizeY);
        _material.SetInt("rez", res);
        _material.SetMatrix("mat", transform.localToWorldMatrix);
        _material.SetVector("position", transform.position);
        _material.SetTexture("tex", outTex);
        
        Graphics.DrawMeshInstancedIndirect(_mesh, 
            0, 
            _material, 
            _bounds, 
            _argBuffer);
    }
}
